package com.ds;

import com.ds.chart.FileLineChart;
import com.ds.chart.LineChart;
import com.ds.form.UploadFromFileSystemFrame;
import com.ds.form.UploadFromInternetFrame;
import org.jfree.ui.RefineryUtilities;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {
    private static List<Double> before = new ArrayList<>();
    private static List<Double> after = new ArrayList<>();
    private static SimpleMovingAverage movingAverage;
    private static FileLineChart lineChart;

    public static void main(String[] args) {
        final JFrame frame = new JFrame("Выбор загрузки файла");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 300);
        JButton button1 = new JButton("из интернета");
        JButton button2 = new JButton("из файла");

        final JPanel panel = new JPanel();
        panel.setLayout(new FlowLayout());
        panel.add(button1);
        panel.add(button2);

        frame.add(panel);
        frame.pack();
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);

        button1.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                new UploadFromInternetFrame();
            }
        });

        button2.addActionListener(new AbstractAction() {
            public void actionPerformed(ActionEvent actionEvent) {
                UploadFromFileSystemFrame upload = new UploadFromFileSystemFrame();
                int result = upload.showOpenDialog(panel);
                if (result == JFileChooser.APPROVE_OPTION) {
                    try {
                        Scanner scanner = new Scanner(upload.getSelectedFile());
                        createDiagramFromFile(scanner);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }

            }
        });
    }

    public static void main(File testFile) throws IOException {
        Scanner scanner = new Scanner(testFile);
        createDiagramToFile(scanner);
    }

    public static void createDiagramFromFile(Scanner scanner) {

        after = new ArrayList<>();

        while (scanner.hasNext()) {
            try {
                after.add(Double.parseDouble(scanner.nextLine()));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
                after.add(0.0);
            }
        }
        System.out.println("Input date: ");
        after.forEach(item -> System.out.print(item + " "));

        before = new ArrayList<>();

        movingAverage = new SimpleMovingAverage(3);
        for (double item : after) {
            movingAverage.addData(item);
            before.add(movingAverage.getMean());
        }

        System.out.println();
        System.out.println();
        System.out.println("Data for checking: ");
        before.forEach(item -> System.out.print(item + " "));

        LineChart lineChart = new LineChart("SMA (before)", after, before);
        lineChart.pack();
        RefineryUtilities.centerFrameOnScreen(lineChart);
        lineChart.setVisible(true);
        scanner.close();
    }

    public static void createDiagramToFile(Scanner scanner) throws IOException {

        after = new ArrayList<>();

        while (scanner.hasNext()) {
            try {
                after.add(Double.parseDouble(scanner.nextLine()));
            } catch (NumberFormatException e) {
                System.out.println(e.getMessage());
                after.add(0.0);
            }
        }
        System.out.println("Input date: ");
        after.forEach(item -> System.out.print(item + " "));

        before = new ArrayList<>();

        movingAverage = new SimpleMovingAverage(3);
        for (double item : after) {
            movingAverage.addData(item);
            before.add(movingAverage.getMean());
        }

        System.out.println();
        System.out.println();
        System.out.println("Data for checking: ");
        before.forEach(item -> System.out.print(item + " "));

        lineChart = new FileLineChart("SMA (before)", after, before);
        lineChart.saveToFile();
        scanner.close();
    }

    public static List<Double> getBefore() {
        return before;
    }

    public static List<Double> getAfter() {
        return after;
    }

    public static SimpleMovingAverage getMovingAverage() {
        return movingAverage;
    }

    public static FileLineChart getLineChart() {
        return lineChart;
    }
}
