package com.ds.chart;

import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartUtilities;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class FileLineChart {
    private final JFreeChart lineChart;

    public FileLineChart(String frameName, List<Double> after, List<Double> before) {
        lineChart = ChartFactory.createLineChart(
                frameName,
                "value",
                "period",
                createDataset(after, before),
                PlotOrientation.VERTICAL,
                true,
                true,
                false
        );
    }

    public void saveToFile() throws IOException {
        ChartUtilities.saveChartAsPNG(new File("pie.png"), lineChart, 500, 500);
    }

    private DefaultCategoryDataset createDataset(List<Double> res, List<Double> before) {
        DefaultCategoryDataset dataset = new DefaultCategoryDataset();

        for (int i = 0; i < res.size(); i++) {
            dataset.addValue(res.get(i), "data", "" + i);
        }

        for (int i = 0; i < before.size(); i++) {
            dataset.addValue(before.get(i), "simple average", "" + i);
        }
        return dataset;
    }
}
