package com.ds;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import java.io.File;
import java.io.IOException;
import java.util.Objects;

public class MainTest {
    @Test
    @DisplayName("Проверка на отсутствие исключений при запуске")
    public void doestThrowException() {
        Assertions.assertDoesNotThrow(() -> Main.main(getFile()));
    }

    @Test
    @DisplayName("Проверка реззультирующего множества значений")
    public void checkAfterValues() throws IOException {
        Main.main(getFile());
        Assertions.assertFalse(Main.getAfter().isEmpty());
    }

    @Test
    @DisplayName("Проверка исходного множества значений")
    public void checkBeforeValues() throws IOException {
        Main.main(getFile());
        Assertions.assertFalse(Main.getBefore().isEmpty());
    }

    @Test
    @DisplayName("Проверка формирования графика")
    public void checkLineChart() throws IOException {
        Main.main(getFile());
        Assertions.assertNotNull(Main.getLineChart());
    }

    @Test
    @DisplayName("Проверка рассчета плавающего среднего значения")
    public void checkMovingAverage() throws IOException {
        Main.main(getFile());
        Assertions.assertNotNull(Main.getMovingAverage());
    }

    @Test
    @DisplayName("Проверка совпадения количества значений исходного множества и результирующего")
    public void checkAfterAndBeforeValuesSize() throws IOException {
        Main.main(getFile());
        Assertions.assertEquals(Main.getBefore().size(), Main.getAfter().size());
    }


    private File getFile() {
        return new File(Objects.requireNonNull(this.getClass().getResource("/test.txt")).getFile());
    }
}
