# Simple Moving Average
Print simple moving average (default: 3 period)

# Demo:

Test data and [Url](https://trello-attachments.s3.amazonaws.com/5c9a07d7c26b2144e193def6/5c9a0a3d1980f07b31651f21/bf901dbfa1910412aae6a1e85145909c/test.txt)
: 2,3,4,1,2,3,5,23,4,42,1,23,2,42,3,4,52,4,2,34,2,34,2,13,24,2,3

Method uploads:

![Method Uploads](./images/1.png)

From url:

![From URL](./images/2.png)

From local disk:

![From local disk](./images/3.png)

Test data for checking:

![Test data](./images/4.png)

Chart for data and simple moving average by 3 period:

![Chart](./images/5.png)

# Input methods:

 * from internet by URL
 * from local disk
 
# Rule input data: 

 * txt document
 * record on new line
 * record is integer or double value
 
# Output 
 * Line char with after and before simple average